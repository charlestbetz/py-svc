#!/bin/bash

if [ -z "$PYSVC_V_1" ] 
then
  echo "PYSVC_V_1 is not set"
else
  echo "killing $PYSVC_V_1"
  docker kill "$PYSVC_V_1"
  unset PYSVC_V_1
  
fi

docker run -d -p 8081:30100 "$LOGNAME/node-svc"
docker ps
export PYSVC_V_1=$(docker ps -a | awk 'FNR == 2 {print $NF}') 
echo "exported" "$PYSVC_V_1" "'to $PYSVC_V_1'"
echo "to kill, 'docker kill $PYSVC_V_1'"


