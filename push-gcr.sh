#!/bin/bash

docker tag $LOGNAME/py-svc gcr.io/$GOOGLE_CLOUD_PROJECT/py-svc:v1
docker push gcr.io/$GOOGLE_CLOUD_PROJECT/py-svc:v1
