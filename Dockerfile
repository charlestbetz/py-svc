FROM python:3.9

# Create app directory
WORKDIR /app

# Copy the pip requirements file
COPY requirements.txt ./

# Install app dependencies
RUN pip install --requirement requirements.txt

# Bundle app source
COPY . /app

# Opens the port which our server will listen on
EXPOSE 30100

CMD [ "python", "./manage.py", "runserver", "--noreload", "0.0.0.0:30100" ]
