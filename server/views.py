import requests
from django.http import HttpResponse
from os import getpid, kill, environ
from time import sleep
from datetime import datetime
import pytz
import signal
import warnings
import asyncio
from asgiref.sync import sync_to_async
import json

# Async code based on example here:
# https://www.valentinog.com/blog/django-q/

PORT = 30100

# Vary these constants according to where you are running (GCS, VMs, K8S) and how many
# VMs you have, if that's the lesson).
arr_nodes = ["localhost"]                               # For testing on GCS
# arr_nodes = ["py-svc-01"]                             # For 1 VM
# arr_nodes = ["py-svc-01", "py-svc-02"]                # For 2 VMs
# arr_nodes = ["py-svc-01", "py-svc-02" , "py-svc-03"]  # For 3 VMs
# arr_nodes = [environ["PY_SVC_PUBLIC_SERVICE_HOST"]]   # Use this for K8S

# Returns JSON with HTTP reguest type and a string combination of
# IP addresss and UTC timestamp.
# Example:
# {
#   "Action": "GET",
#   "arrTimeStamp": ["127.0.0.1 Thu Dec 09 2021 09:50:28 +0000 (UTC)"]}
# }
def make_ip_date_stamp(received_json, ip):
    UTC = pytz.utc
    now = datetime.now(UTC)
    date_time = now.strftime("%a %b %d %Y %H:%M:%S %z (%Z)")
    text = ip + " " + date_time

    if "arrTimeStamp" not in received_json:
        received_json["arrTimeStamp"] = [text]
    else:
        received_json["arrTimeStamp"].append(text)

    return json.dumps(received_json)


# Builds the next URL to send a request to
def build_url(str_level):
    # What is the formula for which node to call?
    # Given x levels and n nodes
    # x % n where x > n, else n

    current_level = int(str_level)
    next_level = current_level - 1
    num_nodes = len(arr_nodes)
    next_node = next_level % num_nodes if next_level >= num_nodes else next_level
    str_url = "http://" + arr_nodes[next_node] + ":" + str(PORT) + "/" + str(next_level)

    return str_url


# Shuts down the django server after one second by sending it a SIGINT signal
@sync_to_async
def shutdown_task(pid):
    # Wait one second so the server can respond to the shutdown request first
    sleep(1)

    # Send SIGINT signal to pid
    kill(pid, signal.SIGINT)


# Handles / and /0
def index(request):
    ip = request.META["REMOTE_ADDR"]

    return HttpResponse(make_ip_date_stamp({"Action": "GET"}, ip), "application/json", 200)


# Handles /<depth> other than /0
def number_get(request, depth):
    ip = request.META["REMOTE_ADDR"]

    next_url = build_url(depth)
    next_response = requests.get(next_url)
    next_response_json = next_response.json()

    return HttpResponse(make_ip_date_stamp(next_response_json, ip), "application/json", 200)


# Handles /999
# Shuts down the server after responding to the request
async def shutdown(request):
    warnings.warn("***SHUTDOWN SIGNAL***")

    response = HttpResponse("Shutting down.", "text/plain", 200)

    # We have to shut the server down asynchronously since the only way to
    # respond is by returning the response.
    asyncio.create_task(shutdown_task(getpid()))

    return response
