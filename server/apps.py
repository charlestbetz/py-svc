from django.apps import AppConfig
from django.http import HttpRequest


class ServerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'server'
    verbose_name = "Server"

    def ready(self):
        import logging
        from . import views

        # Initial smoke test is run here
        logging.log(logging.INFO, "Running smoke test...")

        # Build fake HttpRequest object to pass to view handler
        fake_request = HttpRequest()
        fake_request.META["REMOTE_ADDR"] = "127.0.0.1"

        response = views.index(fake_request)
        logging.log(logging.INFO, "Smoke test response to GET / request: " + str(response.content))
