from django.test import Client, LiveServerTestCase
import requests


class TestServer(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        print('Setting up TestServer class.\nSetting port to 30100.')
        cls.port = 30100
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        print('Tearing down TesServer class.')
        cls.port = None
        super().tearDownClass()

    def test_get_index(self):
        print('Testing GET {0}/'.format(self.live_server_url))
        response = requests.get(self.live_server_url + "/")

        self.assertEqual(200, response.status_code)
        self.assertIn('"Action": "GET"', response.text)
        self.assertIn('"arrTimeStamp"', response.text)

    def test_get_0(self):
        print('Testing GET {0}/0'.format(self.live_server_url))
        response = requests.get(self.live_server_url + "/0")

        self.assertEqual(200, response.status_code)
        self.assertIn('"Action": "GET"', response.text)
        self.assertIn('"arrTimeStamp"', response.text)

    def test_get_n_lt_10(self):
        print('Testing GET {0}/3'.format(self.live_server_url))
        response = requests.get(self.live_server_url + "/3")

        self.assertEqual(200, response.status_code)
        self.assertIn('"Action": "GET"', response.text)
        self.assertIn('"arrTimeStamp"', response.text)

    def test_get_n_gt_10(self):
        print('Testing GET {0}/11'.format(self.live_server_url))
        response = requests.get(self.live_server_url + "/11")
        response_json = response.json()

        self.assertEqual(200, response.status_code)
        self.assertIn('"Action": "GET"', response.text)
        self.assertIn('"arrTimeStamp"', response.text)
        self.assertEqual(len(response_json['arrTimeStamp']), 12)
