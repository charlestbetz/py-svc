from django.urls import path, re_path

from . import views

urlpatterns = [
    path(r'', views.index, name='index'),
    path(r'0', views.index, name='index'),
    path('999', views.shutdown, name='shutdown'),
    re_path(r'(?P<depth>[0-9]+)', views.number_get, name='number_get'),  # All other /<depth> requests
]
